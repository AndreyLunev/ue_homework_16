#define _CRT_SECURE_NO_WARNINGS

#include <ctime>
#include <iostream>

//Function of taking current date
int date()
{
    using namespace std;
	time_t t;
	tm* tk;

	time(&t);
	tk = localtime(&t);

	return tk->tm_mday;
}

int main()
{
    const int N = 10;
    int Array[N][N];
	int Day = date();
	int Sum = 0;

	std::cout << "Now is " << Day << " day of month \n\n";
	std::cout << "Array output \n";

	//Array output
    for (int i=0; i<N; i++)
    {
	    for (int j=0; j<N; j++)
	    {
			std::cout << (Array[i][j] = i+j) << ' '; //array output
	    }
        std::cout << '\n';
    }

	std::cout << '\n';
    std::cout << "Sum output with conditions \n\n";

	//Sum output with conditions 
	for (int i = 0; i < N; i++)
	{
		if (Day % N == i)
		{
			std::cout << "Sum of " << i + 1 << " line is \n";
			for (int j = 0; j < N; j++)
			{
				Sum += Array[i][j];
			}
			std::cout << Sum << '\n';
		}
	}
	
}